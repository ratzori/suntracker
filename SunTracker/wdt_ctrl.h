#ifndef WDT_CTRL_H_
#define WDT_CTRL_H_

void wdt_init(void);
void wdt_soft_reset(void);

#endif /* WDT_CTRL_H_ */