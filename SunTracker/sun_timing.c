Nousu = 12.0 + eqtime/60.0 - fo/15.0 + tzr - lon/15.0 + SummerTime;
Lasku = 12.0 + eqtime/60.0 + fo/15.0 + tzr - lon/15.0 + SummerTime;


g = range(357.528 * rads + .9856003 * rads * jday);
obliq = 23.439 * rads - .0000004 * rads * jday;
L = range(280.461 * rads + .9856474 * rads * jday);

lambda = range(L + 1.915 * rads * Math.sin(g) + 0.02 * rads * Math.sin(2 * g));
obliq = 23.439 * rads - .0000004 * rads * jday;

alpha = Math.atan2(Math.cos(obliq) * Math.sin(lambda), Math.cos(lambda));
eqtime = 1440.0 -720.0 *(L - alpha)/pi;
if (eqtime>1440.0) eqtime-= 1440.0;

function range(x) {
    var b = x /pi/2.0;
    var a = 2*pi * (b - Math.round(b));
    if (a < 0) a = 2*pi + a;
    return a;
}