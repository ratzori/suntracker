#include <stdio.h>
#include <avr/wdt.h>
#include "wdt_ctrl.h"

void wdt_init(void)
    {
    MCUSR = 0;
    wdt_disable();
    }

void wdt_soft_reset(void)
    {
    printf("Error occurred, resetting device.\r\n");
    wdt_enable(WDTO_15MS);

    for(;;)
        {
        /* Wait for WDT reset */
        }
    }