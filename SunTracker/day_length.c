#include "day_length.h"

/* 1. first calculate the day of the year */
float sun_rise_set(float latitude, float longitude, uint8_t rising)
{    
    cosZenith = -0.01454;
    localOffset = -4;
    
    /*year = 2012;
    month = 3;
    day = 17;
    
    N1 = floor(275 * month / 9);
    N2 = floor((month + 9) / 12);
    N3 = (1 + floor((year - 4 * floor(year / 4) + 2) / 3));
    N = N1 - (N2 * N3) + day - 30;*/
    
    N = 176;
    
    /*Example:
    N1 = 183
    N2 = 1
    N3 = 1 + floor((1990 - 4 * 497 + 2) / 3)
       = 1 + floor((1990 - 1988 + 2) / 3)
       = 1 + floor((1990 - 1988 + 2) / 3)
       = 1 + floor(4 / 3)
       = 2
    N = 183 - 2 + 25 - 30 = 176*/
    
    
    /*    N1 = floor(275 * month / 9)
    N2 = floor((month + 9) / 12)
    N3 = (1 + floor((year - 4 * floor(year / 4) + 2) / 3))
    N = N1 - (N2 * N3) + day - 30
    
    Example:
    N1 = 183
    N2 = 1
    N3 = 1 + floor((1990 - 4 * 497 + 2) / 3)
       = 1 + floor((1990 - 1988 + 2) / 3)
       = 1 + floor((1990 - 1988 + 2) / 3)
       = 1 + floor(4 / 3)
       = 2
    N = 183 - 2 + 25 - 30 = 176 */

/* 2. convert the longitude to hour value and calculate an approximate time */
    lng_hour = longitude / 15.0;
    
    if (rising)
        {
        t = N + ((6.0 - lng_hour) / 24.0);
        }
    else
        {
        t = N + ((18.0 - lng_hour) / 24.0);
        }      
    /*Example:
    lngHour = -74.3 / 15 = -4.953
    t = 176 + ((6 - -4.953) / 24)
      = 176.456*/

/* 3. calculate the Sun's mean anomaly */
    M = (0.9856 * t) - 3.289;
    M_rad = M*PI/180;

    
    /*Example:
    M = (0.9856 * 176.456) - 3.289
      = 170.626*/

/* 4. calculate the Sun's true longitude
       [Note throughout the arguments of the trig functions
       (sin, tan) are in degrees. It will likely be necessary to
       convert to radians. eg sin(170.626 deg) =sin(170.626*pi/180
       radians)=0.16287] */
    
    /*L = M + (1.916 * sin(M*PI/180)) + (0.020 * sin(2*M*PI/180)) + 282.634;*/
    L = M + (1.916 * sin(M_rad)) + (0.020 * sin(2*M_rad)) + 282.634;
    
    if (L > 360.0 )
        {
        L -= 360.0;
        }
    
    if ( L < 0.0 )
        {
        L += 360.0;
        }
        
    L_rad = L*PI/180;
    
    /*NOTE: L potentially needs to be adjusted into the range [0,360) by adding/subtracting 360
    
    Example:
    L = 170.626 + (1.916 * sin(170.626)) + (0.020 * sin(2 * 170.626)) + 282.634
    = 170.626 + (1.916 * 0.16287) + (0.020 * -0.32141) + 282.634
    = 170.626 + 0.31206 + -0.0064282 + 282.634
    = 453.566 - 360
    = 93.566*/

/* 5a. calculate the Sun's right ascension */
    
    /*RA = atan(0.91764 * tan(L*PI/180));*/
    RA = atan(0.91764 * tan(L_rad));
    
    RA = RA / PI * 180;
    
    if (RA > 360.0 )
    {
    RA -= 360.0;
    }
    
    if ( RA < 0.0 )
    {
    RA += 360.0;
    }
    /*NOTE: RA potentially needs to be adjusted into the range [0,360) by adding/subtracting 360
    
    Example:
    RA = atan(0.91764 * -16.046)
       = atan(0.91764 * -16.046)
       = atan(-14.722)
       = -86.11412*/

/* 5b. right ascension value needs to be in the same quadrant as L */

    Lquadrant  = (floor( L/90.0)) * 90.0;
    RAquadrant = (floor(RA/90.0)) * 90.0;
    RA = RA + (Lquadrant - RAquadrant);
    
    /*Example:
    Lquadrant  = (floor(93.566/90)) * 90
               = 90
    RAquadrant = (floor(-86.11412/90)) * 90
               = -90
    RA = -86.11412 + (90 - -90)
       = -86.11412 + 180
       = 93.886 */

/* 5c. right ascension value needs to be converted into hours */

    RA = RA / 15.0;
    
    /* Example:
    RA = 93.886 / 15
       = 6.259 */

/* 6. calculate the Sun's declination */

    sinDec = 0.39782 * sin(L_rad);
    cosDec = cos(asin(sinDec));
    
    /* Example:
    sinDec = 0.39782 * sin(93.566)
           = 0.39782 * 0.99806
           = 0.39705
    cosDec = cos(asin(0.39705))
           = cos(asin(0.39705))
           = cos(23.394)
           = 0.91780 */

/* 7a. calculate the Sun's local hour angle */

    lat_rad = latitude*PI/180;
    
    cosH = (cosZenith - (sinDec * sin(lat_rad))) / (cosDec * cos(lat_rad));
    
    if (rising && cosH >  1.0)
      /*the sun never rises on this location (on the specified date)*/
      {
          /* return ? */
      }
    if (!rising && cosH < -1.0)
      /*the sun never sets on this location (on the specified date)*/
      {
          /* return ? */
      }
    
    /*Example:
    cosH = (-0.01454 - (0.39705 * sin(40.9))) / (0.91780 * cos(40.9))
         = (-0.01454 - (0.39705 * 0.65474)) / (0.91780 * 0.75585)
         = (-0.01454 - 0.25996) / 0.69372
         = -0.2745 / 0.69372
         = -0.39570*/

/* 7b. finish calculating H and convert into hours */
    
    if (rising)
        {
        H = 360 - (acos(cosH))/PI*180;
        }
    else
        {
        H = (acos(cosH))/PI*180;
        }     
    
    H = H / 15.0;
    
    /* Example:
    H = 360 - acos(-0.39570)
      = 360 - 113.310   [ note result of acos converted to degrees]
      = 246.690
    H = 246.690 / 15
      = 16.446 */

/* 8. calculate local mean time of rising/setting */
    
    T = H + RA - (0.06571 * t) - 6.622;
    
    /* Example:
    T = 16.446 + 6.259 - (0.06571 * 176.456) - 6.622
      = 16.446 + 6.259 - 11.595 - 6.622
      = 4.488 */

/* 9. adjust back to UTC */
    
    UT = T - lng_hour;
    
    if ( UT > 24.0 )
    {
        UT -= 24.0;
    }
    
    if ( UT < 0.0 )
    {
        UT += 24.0;
    }
    
    /* NOTE: UT potentially needs to be adjusted into the range [0,24) by adding/subtracting 24
    
    Example:
    UT = 4.488 - -4.953
       = 9.441
       = 9h 26m */

/* 10. convert UT value to local time zone of latitude/longitude */
    
    localT = UT + localOffset;
    return localT;
    
    /* Example:
    localT = 9h 26m + -4
           = 5h 26m
           = 5:26 am EDT*/
    }    