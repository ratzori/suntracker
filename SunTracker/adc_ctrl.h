#ifndef ADC_CTRL_H_
#define ADC_CTRL_H_

#include <avr/io.h>

void adc_ctrl_init( void );
uint16_t adc_ctrl_get_sample( uint8_t port );
uint16_t adc_ctrl_get_sample_rounded( uint8_t round_factor, uint8_t port );
uint16_t adc_ctrl_scale_to(  uint16_t adc_sample, uint16_t scalefactor );
uint16_t adc_ctrl_inverse( uint16_t adc_sample );

#endif /* #ifndef ADC_CTRL_H_ */