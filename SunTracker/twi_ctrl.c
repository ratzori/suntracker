#include <stdio.h>
#include "twi_ctrl.h"
#include "common_data.h"
#include "wdt_ctrl.h"

void twi_error(uint8_t return_value)
    {
    D( printf("TWI error:%#X\r\n",return_value) );
    }

void twi_reset(void)
    {
    TWCR &= ~(( 1 << TWSTO ) + ( 1 << TWEN ));
    TWCR |= ( 1 << TWEN );
    }

void print_status()
{
    printf(" Status: 0x%x ",TWSR & 0xF8);
}

void twi_init(void)
    {
    //TWBR = 0x0C; //98.743 kHz @ 11MHz
    TWBR = 0x0E; //86.4 kHz @ 11MHz
    TWSR = 0x01; //jakaja 4
    TWDR = 0xff;
    }

uint8_t twi_read_data(uint8_t is_last)
    {

    twi_busy_check();

    if (is_last)
        {
        TWCR = (1<<TWINT) | (1<<TWEN) | (0<<TWEA);    //luetaan ja annetaan NACK
        }
    else
        {
        TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWEA);    //luetaan ja annetaan ACK
        }

    twi_busy_check();

    D( print_status() );

    return TWDR;
    }

void twi_write_data(uint8_t data)
{
    twi_busy_check();

    TWDR = data;
    TWCR = (1<<TWINT) | (1<<TWEN); /* Sending */

    twi_busy_check();
}


void twi_stop(void)
    {
    twi_busy_check();

    /* Transmit STOP condition */
    TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWSTO);
    }

void twi_repeated_start(uint8_t slave_address)
{
    TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);

    /* Wait for TWINT Flag set. This indicates that the START condition
     * has been transmitted
     */
    twi_busy_check();
}

void twi_start(uint8_t slave_address)
    {
    uint8_t return_value;

    /* send start condition */
    TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);

    /* Wait for TWINT Flag set. This indicates that the START condition
     * has been transmitted
     */
    twi_busy_check();

    /* Check value of TWI Status Register. Mask  prescaler bits.
     * If status different from START go to ERROR
     */

    return_value = TWSR & 0xF8;

    if (return_value != TWI_START)
        {
        twi_error(return_value);
        }

    /* Load SLA_W into TWDR Register. Clear TWINT bit in TWCR to
     * start transmission of address
     */
    TWDR = slave_address;
    TWCR = (1<<TWINT) | (1<<TWEN);

    /* Wait for TWINT Flag set. This indicates that the DATA has
     * been transmitted, and ACK/NACK has been received
     */
    twi_busy_check();

    /* Check value of TWI Status Register. Mask prescaler bits.
     * If status different from MT_DATA_ACK go to ERROR
     */

    return_value = TWSR & 0xF8;

    if (return_value != TWI_MTX_ADR_ACK)
        {
        twi_error(return_value);
        }
    }

void twi_busy_check(void)
    {
    uint16_t i = 0;

    /* Avoid forever-loop */
    while ( ( i < TWI_TWINT_MAX_WAIT_ROUNDS ) &&
        (!(TWCR & (1<<TWINT))))
        {
        i++;
        }

    if ( TWI_TWINT_MAX_WAIT_ROUNDS == i )
        {
        wdt_soft_reset();
        }

    D( printf("Complete round: %i\r\n",i) );

    }