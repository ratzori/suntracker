#include "motor_ctrl.h"
#include "adc_ctrl.h"
#include "timer_ctrl.h"
#include "relay_ctrl.h"
#include "usart_ctrl.h"

uint16_t convert_to_motor_angle(uint16_t target_azimuth)
    {
    /* Limit 360 degrees range to 270 degrees */

    if (target_azimuth < 45)
        {
        return 0;
        }
    else if (target_azimuth > 270)
        {
        return 270;
        }

    return (target_azimuth - 45);
    }

uint16_t convert_to_motor_angle_adc(uint16_t motor_angle)
    {
    uint32_t target_adc;

    target_adc = 102300/270*motor_angle/100;

    return ( uint16_t ) target_adc;
    }


uint16_t motor_get_safe_adc_pos(uint16_t target_adc_pos)
    {
    if (target_adc_pos > (1023 - pot_safe_margin) )
        {
        return 1023 - pot_safe_margin;
        }
    else if (target_adc_pos < pot_safe_margin)
        {
        return pot_safe_margin;
        }

    return target_adc_pos;
    }

void motor_set_safe_margin (uint8_t new_safe_margin)
    {
    pot_safe_margin = new_safe_margin;
    }

void motor_set_hysteresis (uint8_t new_hysteresis)
    {
    hysteresis = new_hysteresis;
    }

uint8_t motor_set_position(uint16_t new_target_pos)
    {
    timer_timeout_req(10);

    while (!timer_timeout)
        {
        curr_pos = adc_ctrl_get_sample_rounded(8,0);

        pos_diff = curr_pos - new_target_pos;

        if ( pos_diff < 0 )
            {
            pos_diff *= -1;

            relay_set(1,OFF);

            if (curr_pos >= new_target_pos)
                {
                relay_set(0,OFF);
                relay_set(1,OFF);
                printf("OK\r\n");
                return MOTOR_SET_OK; /* ok */
                }
            else
                {
                if (pos_diff > hysteresis)
                    {
                    relay_set(0,ON);
                    }
                }
            }
        else
            {
            if (curr_pos <= new_target_pos)
                {
                relay_set(0,OFF);
                relay_set(1,OFF);
                printf("OK\r\n");
                return MOTOR_SET_OK; /* ok */
                }
            else {
                if(pos_diff > hysteresis)
                    {
                    relay_set(1,ON);
                    relay_set(0,ON);
                    }
                }
            }
        }
    relay_set(0,OFF);
    relay_set(1,OFF);

#ifdef LANG_FINNISH
        printf("ei kaantynyt.\r\n");
#else /* #ifdef LANG_FINNISH */
        printf("NOT OK.\r\n");
#endif /* #ifdef LANG_FINNISH */
    return MOTOR_SET_TIMEOUT;
    }