#ifndef RELAY_CTRL_H_
#define RELAY_CTRL_H_

#include <avr/io.h>
#define INVERT 2
#define ON 1
#define OFF 0

void relay_init(void);
void relay_set(uint8_t relay, uint8_t state);


#endif /* #ifndef RELAY_CTRL_H_ */