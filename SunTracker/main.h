/*
 * main.h
 *
 * Created: 5.5.2012 18:43:28
 *  Author: razor
 */


#ifndef MAIN_H_
#define MAIN_H_
#include "common_data.h"

#define DONT_MOVE_PANEL 0
#define MOVE_PANEL 1

void port_init(void);
void init_devices(STM_DATA_STR* main_stm_state_ptr);
uint8_t stm_update_tracker_data(void);
void stm_move_panel(void);
void stm_sleep(uint16_t sleep_time_sec);
void stm_setup(void);
void stm_handle_message(void);
void stm_handle_setup_message(void);

STM_DATA_STR this;

#endif /* MAIN_H_ */