#ifndef DS1307_H_
#define DS1307_H_

#include <avr/io.h>

#define TWI_NACK 1
#define TWI_ACK 0

#define DS1307_WA 0xd0
#define DS1307_RA 0xd1

void ds1307_date_get(
    uint8_t* second,
    uint8_t* minute,
    uint8_t* hour,
    uint8_t* day_of_week,
    uint8_t* day_of_month,
    uint8_t* month,
    uint16_t* year
    );

void ds1307_date_set(
    uint8_t* second,
    uint8_t* minute,
    uint8_t* hour,
    uint8_t* day_of_week,
    uint8_t* day_of_month,
    uint8_t* month,
    uint16_t* year
    );

void ds1307_date_min_set(
    uint8_t* day_of_month,
    uint8_t* month,
    uint16_t* year
    );

void ds1307_clock_set(
    uint8_t* second,
    uint8_t* minute,
    uint8_t* hour
    );

void ds1307_control_register_write(uint8_t control_register);
uint8_t ds1307_control_register_read(void);
void ds1307_ram_write(uint8_t ram_address,uint8_t data);
uint8_t ds1307_ram_read(uint8_t ram_address);

#endif /* #ifndef DS1307_H_ */