#include <stdio.h>
#include "ds1307.h"
#include "twi_ctrl.h"

uint8_t bcd_to_dec(uint8_t bcd_val)
    {
    return ( (bcd_val/16*10) + (bcd_val%16) );
    }

uint8_t dec_to_bcd(uint8_t dec_val)
    {
    return ( (dec_val/10*16) + (dec_val%10) );
    }


// Gets the date and time from the ds1307
void ds1307_date_get(
    uint8_t* second,
    uint8_t* minute,
    uint8_t* hour,
    uint8_t* day_of_week,
    uint8_t* day_of_month,
    uint8_t* month,
    uint16_t* year
    )
    {
    twi_start(DS1307_WA);
    twi_write_data(0); /* read from mem 0 */
    twi_start(DS1307_RA);
    *second = bcd_to_dec(twi_read_data(TWI_ACK) & 0x7f);
    *minute = bcd_to_dec(twi_read_data(TWI_ACK));
    *hour = bcd_to_dec(twi_read_data(TWI_ACK) & 0x3f);
    *day_of_week = bcd_to_dec(twi_read_data(TWI_ACK));
    *day_of_month = bcd_to_dec(twi_read_data(TWI_ACK));
    *month = bcd_to_dec(twi_read_data(TWI_ACK));
    *year = bcd_to_dec(twi_read_data(TWI_NACK));
    *year += 2000;
    twi_stop();
    }

void ds1307_date_set(
    uint8_t* second,
    uint8_t* minute,
    uint8_t* hour,
    uint8_t* day_of_week,
    uint8_t* day_of_month,
    uint8_t* month,
    uint16_t* year
    )
    {
    twi_start(DS1307_WA);
    twi_write_data(0); //start from address 0
    twi_write_data(dec_to_bcd(*second));
    twi_write_data(dec_to_bcd(*minute));
    twi_write_data(dec_to_bcd(*hour));
    twi_write_data(*day_of_week);
    twi_write_data(dec_to_bcd(*day_of_month));
    twi_write_data(dec_to_bcd(*month));
    twi_write_data(dec_to_bcd((uint8_t)(*year-2000)));
    twi_stop();
    }

void ds1307_date_min_set(
    uint8_t* day_of_month,
    uint8_t* month,
    uint16_t* year
    )
    {
    twi_start(DS1307_WA);
    twi_write_data(4); //start from address 4
    twi_write_data(dec_to_bcd(*day_of_month));
    twi_write_data(dec_to_bcd(*month));
    twi_write_data(dec_to_bcd((uint8_t)(*year-2000)));
    twi_stop();
    }

void ds1307_clock_set(
    uint8_t* second,
    uint8_t* minute,
    uint8_t* hour
    )
    {
    twi_start(DS1307_WA);
    twi_write_data(0); //start from address 0
    twi_write_data(dec_to_bcd(*second));
    twi_write_data(dec_to_bcd(*minute));
    twi_write_data(dec_to_bcd(*hour));
    twi_stop();
    }

void ds1307_control_register_write(uint8_t control_register)
    {
    twi_start(DS1307_WA);
    twi_write_data(7); //start from address 7
    twi_write_data(control_register);
    twi_stop();
    }

uint8_t ds1307_control_register_read(void)
    {
    uint8_t control_register;

    twi_start(DS1307_WA);
    twi_write_data(7); /* read from mem 0 */
    twi_start(DS1307_RA);
    control_register = twi_read_data(TWI_NACK);
    twi_stop();

    return control_register;
    }

void ds1307_ram_write(uint8_t ram_address,uint8_t data)
    {
    /* Last memory is 0x37+0x08 = 0x3F */
    if ( ram_address > 0x37 )
        {
        ram_address = 0x37;
        }

    twi_start(DS1307_WA);
    twi_write_data(0x08+ram_address); //ram 08h-03f
    twi_write_data(data);
    twi_stop();
    }

uint8_t ds1307_ram_read(uint8_t ram_address)
    {
    uint8_t data;

    /* Last memory is 0x37+0x08 = 0x3F */
    if ( ram_address > 0x37 )
        {
        ram_address = 0x37;
        }

    twi_start(DS1307_WA);
    twi_write_data(0x08+ram_address); /* read from mem 0 */
    twi_start(DS1307_RA);
    data = twi_read_data(TWI_NACK);
    twi_stop();

    return data;
    }