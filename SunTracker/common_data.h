/*
 * IncFile1.h
 *
 * Created: 14.6.2012 18:19:18
 *  Author: razor
 */


#ifndef COMMON_DATA_H_
#define COMMON_DATA_H_
#include <avr/io.h>

#define STM_STATE_INIT 0
#define STM_STATE_CHECK_DATE 1
#define STM_STATE_CHECK_BRIGHTNESS 2
#define STM_STATE_UPDATE_TRACKER 3
#define STM_STATE_MOVE_PANEL 4
#define STM_STATE_SLEEP 5
#define STM_STATE_SETUP 6
#define STM_STATE_NEW_MSG 7
#define STM_STATE_USER_WRITING 8

/* Define memory locations for data in ds1307 */
#define MEM_LATITUDE_LOW 0x00
#define MEM_LATITUDE_HIGH 0x01
#define MEM_LONGITUDE_LOW 0x02
#define MEM_LONGITUDE_HIGH 0x03
#define MEM_TURN_INTERVAL 0x04
#define MEM_BRIGHTNESS 0x05
#define MEM_FIRST_WORKING_MONTH 0x06
#define MEM_LAST_WORKING_MONTH 0x07

#define MSG_BUFFER_SIZE (80+1) /* + NULL */

#define TRUE 1
#define FALSE 0

#define SUN_RISING 1
#define SUN_SETTING 0

#define LANG_FINNISH

#define USER_INPUT_TIMEOUT 120 /* 2 min */

/* #define DEBUG */

#ifdef DEBUG
#define D(x) x
#else /* #ifdef DEBUG */
#define D(x)
#endif /* #ifdef DEBUG */

typedef int16_t int16_q7;

typedef struct
    {
    uint8_t stm_state;
    uint8_t stm_prev_state;

    int16_q7 longitude;
    int16_q7 latitude;

    uint8_t first_working_month;
    uint8_t last_working_month;

    uint8_t second;
    uint8_t minute;
    uint8_t hour;
    uint8_t day_of_week;
    uint8_t day_of_month;
    uint16_t day_of_year;
    uint8_t month;
    uint16_t year;

    uint8_t minimum_brightness;

    uint16_t sun_rise_time;
    uint16_t sun_set_time;
    uint16_t rise_azimuth;
    uint16_t set_azimuth;
    uint32_t day_lenght;

    uint16_t curr_time;
    uint32_t full_tracking_angle;
    uint32_t target_angle;
    uint8_t turn_interval;

    uint16_t motor_angle;
    uint16_t previous_motor_angle;
    uint16_t motor_angle_adc;

    uint8_t user_writing;
    uint16_t writing_started_mmss;
    uint16_t current_mmss;
    uint8_t new_message;
    char message[MSG_BUFFER_SIZE];
    } STM_DATA_STR;


#endif /* COMMON_DATA_H_ */