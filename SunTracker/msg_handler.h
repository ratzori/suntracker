#ifndef MSG_HANDLER_H_
#define MSG_HANDLER_H_

#include "common_data.h"

void msg_handler_clear_message(STM_DATA_STR* stm_ptr);
void msg_handler_ctrl_message(STM_DATA_STR* stm_ptr);
void msg_handler_setup_message(STM_DATA_STR* stm_ptr);

#endif /* MSG_HANDLER_H_ */