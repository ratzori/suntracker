#ifndef MSG_DECODER_H_
#define MSG_DECODER_H_
#include "common_data.h"

uint8_t decode_gps(char* message,int16_q7* latitude,int16_q7* longitude);
uint8_t decode_clk(char* message,uint8_t* sec,uint8_t* min,uint8_t* hour);
uint8_t decode_day(char* message,uint8_t* day,uint8_t* month,uint16_t* year);
uint8_t decode_turn_intrv(char* message,uint8_t* turn_interval);
uint8_t decode_brightness(char* message,uint8_t* brightness);
uint8_t decode_worktime(char* message,uint8_t* first_working_month,uint8_t* last_working_month);
uint8_t calculate_digits(char* message,uint8_t char_nbr);
uint32_t string_to_int(char* str,uint8_t* cursor,uint8_t* nbr_of_digits);
void clean_str(char* str, uint8_t length);
uint8_t is_number(uint8_t character);

#endif /* MSG_DECODER_H_ */