#include <stdio.h>
#include <string.h>
#include "msg_handler.h"
#include "common_data.h"
#include "ds1307.h"
#include "msg_decoder.h"

void msg_handler_clear_message(STM_DATA_STR* stm_ptr)
    {
    uint8_t i;

    for (i=0; i<MSG_BUFFER_SIZE; i++)
        {
        stm_ptr->message[i] = '\0';
        }
    stm_ptr->new_message = FALSE;
    }

void msg_handler_ctrl_message(STM_DATA_STR* stm_ptr)
    {

    if ( ( strcmp("SETUP",stm_ptr->message) == 0 ) ||
        ( strcmp("ASETUS",stm_ptr->message) == 0 ) )
        {
#ifdef LANG_FINNISH
        printf("Asetustila.\r\n");
#else /* #ifdef LANG_FINNISH */
        printf("Setup mode.\r\n");
#endif /* #ifdef LANG_FINNISH */
        D( printf("Entering setup-mode\r\n") );
        stm_ptr->stm_state = STM_STATE_SETUP;
        }
    else if ( ( strcmp("RUN",stm_ptr->message) == 0 ) ||
        ( strcmp("AJO",stm_ptr->message) == 0 ) )
        {
        /* Initialize */
        stm_ptr->stm_state = STM_STATE_INIT;
        }
    else if ( stm_ptr->stm_prev_state == STM_STATE_SETUP )
        {
        /* Handle device setup messages if any */
        msg_handler_setup_message(stm_ptr);
        stm_ptr->stm_state = stm_ptr->stm_prev_state;
        }
    else
        {
        if ( strcmp("",stm_ptr->message) != 0 )
            {
#ifdef LANG_FINNISH
        printf("Tuntematon komento.\r\n");
#else /* #ifdef LANG_FINNISH */
        printf("Unknown command.\r\n");
#endif /* #ifdef LANG_FINNISH */
            }

        /* Could not handle message, continue normally */
        if ( stm_ptr->stm_prev_state != STM_STATE_NEW_MSG )
            {
            stm_ptr->stm_state = stm_ptr->stm_prev_state;
            }
        else
            {
            stm_ptr->stm_state = STM_STATE_INIT;
            }
        }

    msg_handler_clear_message(stm_ptr);
    }

void msg_handler_setup_message(STM_DATA_STR* stm_ptr)
    {
    uint8_t temp_hour;
    uint8_t temp_min;
    uint8_t temp_sec;
    uint8_t temp_day;
    uint8_t temp_month;
    uint16_t temp_year;
    uint8_t temp_turn_interval;
    uint8_t temp_brightness;
    uint8_t temp_first_working_month;
    uint8_t temp_last_working_month;

    if ( strncmp("GPS",stm_ptr->message,3) == 0 )
        {
        D( printf("Command is GPS\r\n") );
        if ( decode_gps(stm_ptr->message,&stm_ptr->latitude,&stm_ptr->longitude) == TRUE )
            {
#ifdef LANG_FINNISH
            printf("Uusi GPS sijainti: (huom. pyoristys)\r\n");
#else /* #ifdef LANG_FINNISH */
            printf("New GPS data: (note rounding)\r\n");
#endif /* #ifdef LANG_FINNISH */

            /* copy to memory */
            printf("Latitude: %i.%i\r\n",(stm_ptr->latitude / 128 ),(uint16_t)( ((uint32_t)stm_ptr->latitude*100/128 ) - (stm_ptr->latitude/128*100) ) );
            printf("Longitude: %i.%i\r\n", ( stm_ptr->longitude / 128 ),(uint16_t)( ((uint32_t)stm_ptr->longitude*100/128 ) - (stm_ptr->longitude/128*100) ) );
            D( printf("Saving to DS1307...\r\n") );

            ds1307_ram_write(MEM_LATITUDE_LOW, (uint8_t) stm_ptr->latitude);
            ds1307_ram_write(MEM_LATITUDE_HIGH, (uint8_t) ( stm_ptr->latitude >> 8 ) );
            ds1307_ram_write(MEM_LONGITUDE_LOW, (uint8_t) stm_ptr->longitude);
            ds1307_ram_write(MEM_LONGITUDE_HIGH, (uint8_t) ( stm_ptr->longitude >> 8 ) );
            }
        }
    else if( strncmp("CLK",stm_ptr->message,3) == 0 )
        {
        D( printf("Command CLK\r\n") );
        if ( decode_clk(stm_ptr->message,&temp_sec,&temp_min,&temp_hour) == TRUE )
            {
#ifdef LANG_FINNISH
            printf("Uusi kellonaika: %02d:%02d:%02d.\r\n",temp_hour,temp_min,temp_sec);
#else /* #ifdef LANG_FINNISH */
            printf("Clock time updated to: %02d:%02d:%02d.\r\n",temp_hour,temp_min,temp_sec);
#endif /* #ifdef LANG_FINNISH */
            D( printf("Saving to DS1307...\r\n") );
            ds1307_clock_set(&temp_sec,&temp_min,&temp_hour);
            }
        }
    else if( ( strncmp("DATE",stm_ptr->message,4) == 0 ) ||
        ( strncmp("PVM",stm_ptr->message,3) == 0 ) )
        {
        D( printf("Command DAT\r\n") );
        if ( decode_day(stm_ptr->message,&temp_day,&temp_month,&temp_year) == TRUE )
            {
#ifdef LANG_FINNISH
            printf("Uusi pvm: %i.%i.%i.\r\n",temp_day,temp_month,temp_year);
#else /* #ifdef LANG_FINNISH */
            printf("New date: %i.%i.%i.\r\n",temp_day,temp_month,temp_year);
#endif /* #ifdef LANG_FINNISH */
            D( printf("Saving to DS1307...\r\n") );
            ds1307_date_min_set(&temp_day,&temp_month,&temp_year);
            }
        }
    else if( ( strncmp("INTERVAL",stm_ptr->message,8) == 0 ) ||
        ( strncmp("KAANTOVALI",stm_ptr->message,10) == 0 ) )
        {
        D( printf("Command INTERVAL\r\n") );
        if ( decode_turn_intrv(stm_ptr->message,&temp_turn_interval) == TRUE )
            {
#ifdef LANG_FINNISH
            printf("Uusi kaantovali: %i min.\r\n",temp_turn_interval);
#else /* #ifdef LANG_FINNISH */
            printf("New turn interval: %i min.\r\n",temp_turn_interval);
#endif /* #ifdef LANG_FINNISH */
            D( printf("Saving to DS1307...\r\n") );
            ds1307_ram_write(MEM_TURN_INTERVAL,temp_turn_interval);
            }
        }
    else if( ( strncmp("BRIGHTNESS",stm_ptr->message,10) == 0 ) ||
        ( strncmp("VALOISUUS",stm_ptr->message,9) == 0 ) )
        {
        D( printf("Command BRIGHTNESS\r\n") );
        if ( decode_brightness(stm_ptr->message,&temp_brightness) == TRUE )
            {
#ifdef LANG_FINNISH
            printf("Uusi min kirkkaustaso: %i%%.\r\n",temp_brightness);
#else /* #ifdef LANG_FINNISH */
            printf("New minimum brighness level: %i%%.\r\n",temp_brightness);
#endif /* #ifdef LANG_FINNISH */
            D( printf("Saving to DS1307...\r\n") );

            ds1307_ram_write(MEM_BRIGHTNESS, temp_brightness);
            }
        }
    else if( ( strncmp("WORKTIME",stm_ptr->message,11) == 0 ) ||
        ( strncmp("TOIMINTA",stm_ptr->message,8) == 0 ) )
        {
        D( printf("Command WORKTIME\r\n") );
        if ( decode_worktime(stm_ptr->message,&temp_first_working_month,&temp_last_working_month) == TRUE )
            {
#ifdef LANG_FINNISH
            printf("Uudet toimintakuukaudet: %i.-%i.\r\n",temp_first_working_month,temp_last_working_month);
#else /* #ifdef LANG_FINNISH */
            printf("Uudet working months: %i.-%i.\r\n",temp_first_working_month,temp_last_working_month);
#endif /* #ifdef LANG_FINNISH */
            D( printf("Saving to DS1307...\r\n") );

            ds1307_ram_write(MEM_FIRST_WORKING_MONTH, temp_first_working_month);
            ds1307_ram_write(MEM_LAST_WORKING_MONTH, temp_last_working_month);
            }
        }
    else
        {
#ifdef LANG_FINNISH
        printf("Tuntematon komento.\r\n");
#else /* #ifdef LANG_FINNISH */
        printf("Unknown command.\r\n");
#endif /* #ifdef LANG_FINNISH */
        }
    }