#ifndef DAY_LENGTH
#define DAY_LENGTH

#define PI 3.14159
#define FALSE 0
#define TRUE 1

#include <avr/io.h>
#include <math.h>

float sun_rise_set(float latitude, float longitude, uint8_t rising);

/* 1. */
float N1;
float N2;
float N3;
float N;
float year;
float month;
float day;

/* 2. */
float longitude;
float lng_hour;
float t;
    
/* 3. */
float M;
float M_rad;
    
/* 4. */
float L;
float L_rad;
    
/* 5a. */
float RA;
    
/* 5b. */
float Lquadrant;
float RAquadrant;
    
/* 6 */
float sinDec;
float cosDec;
    
/* 7a */
float cosH;
/* float zenith; */
float cosZenith;
float latitude;
float lat_rad;
    
/* 7b. */
float H;
    
/* 8. */
float T;
    
/* 9. */
float UT;
float lngHour;
    
/* 10. */
float localT;
int8_t localOffset;


#endif /* #ifndef DAY_LENGTH */