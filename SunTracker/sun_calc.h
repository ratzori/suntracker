#ifndef SUN_CALC_H_
#define SUN_CALC_H_

#include <avr/io.h>
#include <math.h>
#include "common_data.h"

#define PI 3.14159

uint16_t get_day_length(uint16_t rise_time, uint16_t set_time);
uint16_t get_day_of_year(uint8_t day, uint8_t month, uint16_t year);
uint8_t get_sun_rise_time(uint8_t rise_or_set, STM_DATA_STR* stm_ptr);

/* 2. */
volatile float lng_hour;
volatile float t;

/* 3. */
volatile float M;
volatile float M_rad;

/* 4. */
volatile float L;
volatile float L_rad;

/* 5a. */
volatile float RA;

/* 5b. */
volatile float Lquadrant;
volatile float RAquadrant;

/* 6 */
volatile float sinDec;
volatile float cosDec;
volatile float cosAz;

/* 7a */
volatile float cosH;
volatile float cosZenith;
volatile float lat_rad;

/* 7b. */
volatile float H;
volatile uint8_t sun_rises_or_sets;

/* 7c. */
volatile float cos_zenith_angle;
volatile float sin_zenith_angle;

/* 8. */
volatile float T;

/* 9. */
volatile float UT;
volatile float lngHour;

/* 10. */
//volatile float localT;
//volatile int8_t localOffset;
volatile int8_t sun_rise_hours;
volatile int8_t sun_rise_mins;

#endif /* #ifndef SUN_CALC_H_ */