#include "relay_ctrl.h"

void relay_init(void)
    {
    DDRB |= (1<<PB2) | (1<<PB3); /* for 2 relays */
    PORTB |= (1<<PB2) | (1<<PB3);
    }

void relay_set(uint8_t relay, uint8_t state)
    {
    if(state == INVERT)
        {
        state = PORTB & ((1<<PB2)<<relay); /* gives inverted result */
        }

    if (state == OFF)
        {
        PORTB |= ((1<<PB2)<<relay); /* set to 1 to set relay off */
        }
    else
        {
        PORTB &= ~((1<<PB2)<<relay);
        }
    }