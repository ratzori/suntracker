/*
 * main.c
 *
 * Created: 10.3.2012 10:08:40
 *  Author: razor
 */

#ifndef F_CPU
#define F_CPU 11059200UL
#endif /* F_CPU */

#include <avr/io.h>
#include <avr/sleep.h>
#include <stdio.h>
#include <string.h>
#include "main.h"
#include "adc_ctrl.h"
#include "common_data.h"
#include "ds1307.h"
#include "timer_ctrl.h"
#include "sun_calc.h"
#include "usart_ctrl.h"
#include "motor_ctrl.h"
#include "relay_ctrl.h"
#include "twi_ctrl.h"
#include "msg_handler.h"
#include "wdt_ctrl.h"

#ifdef DEBUG
#include "raz_delay.h"
#endif /* #ifdef DEBUG */

void port_init(void)
    {
    DDRB = 0xfd;
    PORTB = 0x02;
    }

void init_devices(STM_DATA_STR* main_STM_ptr)
    {
    wdt_init();
    timer_init();
    adc_ctrl_init();
    port_init();
    motor_set_hysteresis(12);
    motor_set_safe_margin(10);
    USART_Init(main_STM_ptr);
    relay_init();
    twi_init();
    sei();
    }

uint8_t stm_check_date(void)
    {
    ds1307_date_get(
        &this.second,
        &this.minute,
        &this.hour,
        &this.day_of_week,
        &this.day_of_month,
        &this.month,
        &this.year
        );

    if ( ( this.month >= this.first_working_month ) && ( this.month <= this.last_working_month ) )
        {
        return MOVE_PANEL;
        }
    else
        {
#ifdef LANG_FINNISH
        printf("%02d:%02d:%02d - Kuukausi: %i. (toiminta-aika: %i.-%i.).\r\n",
            this.hour,this.minute,this.second,this.month,this.first_working_month,this.last_working_month );
#else /* #ifdef LANG_FINNISH */
        printf("%02d:%02d:%02d - Month: %i. (working months: %i.-%i.).\r\n",
            this.hour,this.minute,this.second,this.month,this.first_working_month,this.last_working_month );
#endif /* #ifdef LANG_FINNISH */
        return DONT_MOVE_PANEL;
        }
    }

uint8_t stm_check_brightness(void)
    {
    uint16_t curr_brightness;
    curr_brightness = adc_ctrl_inverse(adc_ctrl_get_sample_rounded(8,1));
    curr_brightness = adc_ctrl_scale_to(curr_brightness,100);

    if ( curr_brightness >= this.minimum_brightness )
        {
#ifdef LANG_FINNISH
        printf("%02d:%02d:%02d - Valoisuus: %i%% (min. %i%%) - OK.\r\n",this.hour,this.minute,this.second,curr_brightness,this.minimum_brightness);
#else /* #ifdef LANG_FINNISH */
        printf("%02d:%02d:%02d - Brightness level:%i%% (min. %i%%) - OK\r\n",this.hour,this.minute,this.second,curr_brightness,this.minimum_brightness);
#endif /* #ifdef LANG_FINNISH */
        return MOVE_PANEL;
        }
    else
        {
#ifdef LANG_FINNISH
        printf("%02d:%02d:%02d - Valoisuus alle minimin: %i%% (min.%i%%).\r\n",this.hour,this.minute,this.second,curr_brightness,this.minimum_brightness);
#else /* #ifdef LANG_FINNISH */
        printf("%02d:%02d:%02d - Brightness less than minimum level:%i%% (min.%i%%)\r\n",this.hour,this.minute,this.second,curr_brightness,this.minimum_brightness);
#endif /* #ifdef LANG_FINNISH */
        return DONT_MOVE_PANEL;
        }
    }

uint8_t stm_update_tracker_data(void)
    {
    uint8_t sun_rises;
    uint8_t sun_sets;

    this.day_of_year = get_day_of_year(this.day_of_month,this.month,this.year);

    sun_rises = get_sun_rise_time(SUN_RISING,&this);

    if (sun_rises)
        {
        this.curr_time = ((this.hour)*60 + this.minute);
        D( printf("Curr time: %i = %02d:%02d\r\n",this.curr_time,this.hour,this.minute) );

        sun_sets = get_sun_rise_time(SUN_SETTING, &this);
        if (sun_sets)
            {
            this.day_lenght = get_day_length(this.sun_rise_time,this.sun_set_time);
            }
        else
            {
            this.day_lenght = 1440;
            }

        D( printf("day_lenght %i\r\n", (int) this.day_lenght) );
        D( printf("sun rise %i\r\n", this.sun_rise_time) );
        D( printf("sun set %i\r\n", this.sun_set_time) );

        this.set_azimuth = 360 - this.rise_azimuth;
        this.full_tracking_angle = this.set_azimuth - this.rise_azimuth;

        return MOVE_PANEL;
        }
    else
        {
        return DONT_MOVE_PANEL;
        }

    }

void stm_move_panel(void)
    {
    uint32_t curr_time_fixed;

    /* Check if rise time needs to correct with 24h */
    if (this.curr_time <= this.sun_rise_time)
        {
        curr_time_fixed = this.curr_time + 1440;
        }
    else
        {
        curr_time_fixed = this.curr_time;
        }

    this.target_angle = (this.full_tracking_angle*1000/this.day_lenght) * (curr_time_fixed-this.sun_rise_time) / 1000 + this.rise_azimuth;

    D( printf("Target: %i\r\n",(int) this.target_angle) );
    D( printf("Rise az: %i\r\n",this.rise_azimuth) );
    D( printf("Full tracking: %i\r\n",(int) this.full_tracking_angle) );
    D( printf("current time %i\r\n",this.curr_time) );

    this.motor_angle = convert_to_motor_angle(this.target_angle);

    if (this.motor_angle != this.previous_motor_angle)
        {
        D( printf("Move panel\r\n") );
        this.motor_angle_adc = convert_to_motor_angle_adc(this.motor_angle);
        D( printf("Motor: %i / %i\r\n",this.motor_angle,this.motor_angle_adc) );
#ifdef LANG_FINNISH
        printf("%02d:%02d:%02d - Paneelin uusi suunta: %i - ",this.hour,this.minute,this.second,(int) this.target_angle);
#else /* #ifdef LANG_FINNISH */
        printf("%02d:%02d:%02d - New direction for panel: %i - ",this.hour,this.minute,this.second,(int) this.target_angle);
#endif /* #ifdef LANG_FINNISH */
        motor_set_position(this.motor_angle_adc);
        this.previous_motor_angle = this.motor_angle;
        }
    else
        {
#ifdef LANG_FINNISH
        printf("%02d:%02d:%02d - Paneelin suunta OK.\r\n",this.hour,this.minute,this.second);
#else /* #ifdef LANG_FINNISH */
        printf("%02d:%02d:%02d - Panel direction OK.\r\n",this.hour,this.minute,this.second);
#endif /* #ifdef LANG_FINNISH */
        D( printf("Motor angle not changed\r\n") );
        }
    }

void stm_sleep(uint16_t sleep_time_sec)
    {
    ds1307_date_get(
        &this.second,
        &this.minute,
        &this.hour,
        &this.day_of_week,
        &this.day_of_month,
        &this.month,
        &this.year
        );
#ifdef LANG_FINNISH
        printf("%02d:%02d:%02d - Lepotilaan %i minuutiksi.\r\n",this.hour,this.minute,this.second,(sleep_time_sec/60));
#else /* #ifdef LANG_FINNISH */
        printf("%02d:%02d:%02d - Sleep mode for %i minutes.\r\n",this.hour,this.minute,this.second,sleep_time_sec/60);
#endif /* #ifdef LANG_FINNISH */
    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_enable();
    timer_timeout_req(sleep_time_sec);
    sleep_cpu();
    D( printf("STM: Wake up\r\n") );
    sleep_disable();

    twi_reset();

    ds1307_date_get(
        &this.second,
        &this.minute,
        &this.hour,
        &this.day_of_week,
        &this.day_of_month,
        &this.month,
        &this.year
        );
#ifdef LANG_FINNISH
        printf("%02d:%02d:%02d - Takaisin toimintatilaan.\r\n",this.hour,this.minute,this.second);
#else /* #ifdef LANG_FINNISH */
        printf("%02d:%02d:%02d - Back to normal mode.\r\n",this.hour,this.minute,this.second);
#endif /* #ifdef LANG_FINNISH */
    }

void stm_user_writing(void)
    {
    ds1307_date_get(
        &this.second,
        &this.minute,
        &this.hour,
        &this.day_of_week,
        &this.day_of_month,
        &this.month,
        &this.year
        );

    this.current_mmss = this.minute * 60; /* to seconds */
    this.current_mmss += this.second;

    if (this.current_mmss < this.writing_started_mmss)
        {
        this.current_mmss += 3600;
        }

    /* Timeout USER_INPUT_TIMEOUT seconds */
    if ( (this.current_mmss - this.writing_started_mmss) >= USER_INPUT_TIMEOUT )
        {
#ifdef LANG_FINNISH
        printf("\r\n%02d:%02d:%02d - Aikaraja umpeutui.\r\n",this.hour,this.minute,this.second);
#else /* #ifdef LANG_FINNISH */
        printf("\r\n%02d:%02d:%02d - Timeout.\r\n",this.hour,this.minute,this.second);
#endif /* #ifdef LANG_FINNISH */
        /* Clean buffer (sets user_writing to false) */
        USART_buffer_clear();
        }

    if ( this.user_writing == FALSE )
        {
        if ( this.stm_prev_state != STM_STATE_USER_WRITING )
            {
            this.stm_state = this.stm_prev_state;
            }
        else
            {
            this.stm_state = STM_STATE_INIT;
            }
        }
    }

void stm_setup(void)
    {

    }

int main()
    {
    this.stm_state = STM_STATE_INIT;

    while(1)
        {
        switch (this.stm_state)
            {
            case STM_STATE_INIT:
                init_devices(&this);
                D( printf("STM: Init\r\n") );

                this.previous_motor_angle = 361; /* Value that convert_to_motor_angle won't return */

                this.latitude = ds1307_ram_read(MEM_LATITUDE_LOW);
                this.latitude += ds1307_ram_read(MEM_LATITUDE_HIGH) << 8;

                /* Check that latitude is between 90.00 and -90.00 */
                if ( (this.latitude > 11520) || ( this.latitude < -11520 ) )
                    {
                    this.latitude = 8320; /* 65.00 */
                    ds1307_ram_write(MEM_LATITUDE_LOW, (uint8_t) this.latitude);
                    ds1307_ram_write(MEM_LATITUDE_HIGH, (uint8_t) ( this.latitude >> 8 ) );
                    }

                this.longitude = ds1307_ram_read(MEM_LONGITUDE_LOW);
                this.longitude += ds1307_ram_read(MEM_LONGITUDE_HIGH) << 8;

                /* Check that longitude is between 180.00 and -180.00 */
                if ( (this.longitude > 23040) || ( this.longitude < -23040 ) )
                    {
                    this.longitude = 3264; /* 25.50 */
                    ds1307_ram_write(MEM_LONGITUDE_LOW, (uint8_t) this.longitude);
                    ds1307_ram_write(MEM_LONGITUDE_HIGH, (uint8_t) ( this.longitude >> 8 ) );
                    }

                D( printf("Latitude: %i\r\n",this.latitude) );
                D( printf("Longitude: %i\r\n",this.longitude) );

                this.turn_interval = ds1307_ram_read(MEM_TURN_INTERVAL);

                this.minimum_brightness = ds1307_ram_read(MEM_BRIGHTNESS);

                if ( this.minimum_brightness > 100 )
                    {
                    this.minimum_brightness = 0;
                    ds1307_ram_write(MEM_BRIGHTNESS, this.minimum_brightness);
                    }

                this.first_working_month = ds1307_ram_read(MEM_FIRST_WORKING_MONTH);

                if ( ( this.first_working_month == 0 || this.first_working_month > 12 ) )
                    {
                    this.first_working_month = 1;
                    ds1307_ram_write(MEM_FIRST_WORKING_MONTH, this.first_working_month);
                    }

                this.last_working_month = ds1307_ram_read(MEM_LAST_WORKING_MONTH);

                if ( ( this.last_working_month == 0 || this.last_working_month > 12 ) )
                    {
                    this.last_working_month = 12;
                    ds1307_ram_write(MEM_LAST_WORKING_MONTH, this.last_working_month);
                    }

                printf("\r\n");
                msg_handler_clear_message(&this);
                this.stm_state = STM_STATE_CHECK_DATE;
                break;
            case STM_STATE_CHECK_DATE:
                D( printf("STM: Check date\r\n") );
                if (stm_check_date() == MOVE_PANEL)
                    {
                    this.stm_state = STM_STATE_CHECK_BRIGHTNESS;
                    }
                else
                    {
                    this.stm_state = STM_STATE_SLEEP;
                    }
                break;
            case STM_STATE_CHECK_BRIGHTNESS:
                D( printf("STM: Check brightness\r\n") );
                if (stm_check_brightness() == MOVE_PANEL)
                    {
                    this.stm_state = STM_STATE_UPDATE_TRACKER;
                    }
                else
                    {
                    this.stm_state = STM_STATE_SLEEP;
                    }
                break;
            case STM_STATE_UPDATE_TRACKER:
                D( printf("STM: Update tracker data\r\n") );
                if (stm_update_tracker_data() == MOVE_PANEL)
                    {
                    this.stm_state = STM_STATE_MOVE_PANEL;
                    }
                else
                    {
                    this.stm_state = STM_STATE_SLEEP;
                    }
                break;
            case STM_STATE_MOVE_PANEL:
                D( printf("STM: Move panel\r\n") );
                stm_move_panel();
                this.stm_state = STM_STATE_SLEEP;
                break;
            case STM_STATE_SLEEP:
                D( printf("STM: Sleep\r\n") );
                stm_sleep(this.turn_interval * 60); /* x 60 to convert to seconds */
                this.stm_state = STM_STATE_CHECK_DATE;
                break;
            case STM_STATE_SETUP:
                //printf("STM: Setup\r\n");
                stm_setup();
                break;
            case STM_STATE_NEW_MSG:
                D( printf("STM: New message\r\n") );
                msg_handler_ctrl_message(&this);
                break;
            case STM_STATE_USER_WRITING:
                stm_user_writing();
                break;
            default:
                this.stm_state = STM_STATE_INIT;
                break;
            }

            if ( (this.new_message == TRUE) && (this.stm_state != STM_STATE_NEW_MSG) )
                {
                this.stm_prev_state = this.stm_state;
                this.stm_state = STM_STATE_NEW_MSG;
                }
            else if ( (this.user_writing == TRUE) && (this.stm_state != STM_STATE_USER_WRITING) )
                {
                ds1307_date_get(
                    &this.second,
                    &this.minute,
                    &this.hour,
                    &this.day_of_week,
                    &this.day_of_month,
                    &this.month,
                    &this.year
                    );

                this.writing_started_mmss = this.minute * 60; /* to seconds */
                this.writing_started_mmss += this.second;

                this.stm_prev_state = this.stm_state;
                this.stm_state = STM_STATE_USER_WRITING;
                }
#ifdef DEBUG
            printf("State: %i\r\n",this.stm_state);
            ms_delay(2000);
#endif /* #ifdef DEBUG */
        }
    return 0;
    }