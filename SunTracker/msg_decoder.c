#include <avr/io.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "msg_decoder.h"
#include "common_data.h"

uint8_t decode_gps(char* message,int16_q7* latitude,int16_q7* longitude)
    {
    uint8_t i;
    uint8_t nbr_of_digits;

    if ( strncmp("GPS,",message,4) != 0 )
        {
        return FALSE;
        }

    if(!(is_number(message[4])))
        {
        return FALSE;
        }

    i = 3;

    *latitude = string_to_int(message,&i,&nbr_of_digits) * 128; /* to int16_q7 */

    if ( message[i] == '.' )
        {
        *latitude += ( string_to_int(message,&i,&nbr_of_digits) * 128 ) / pow(10,nbr_of_digits);
        }

    i++; /* Jump over "," */

    if ( message[i] == 'S' )
        {
        *latitude *= -1;
        }

    i++; /* Jump over N/S */

    *longitude = string_to_int(message,&i,&nbr_of_digits) * 128; /* to int16_q7 */

    if ( message[i] == '.' )
        {
        *longitude += ( string_to_int(message,&i,&nbr_of_digits) * 128 ) / pow(10,nbr_of_digits);
        }

    i++; /* Jump over "," */

    if ( message[i] == 'W' )
        {
        *longitude *= -1;
        }
    return TRUE;
    }

uint8_t decode_clk(char* message,uint8_t* sec,uint8_t* min,uint8_t* hour)
    {
    uint8_t i;
    uint8_t nbr_of_digits;

    *sec = 0;
    *min = 0;
    *hour = 0;

    if ( ( strncmp("CLK,",message,4) != 0 ) &&
        ( strncmp("KLO,",message,4) != 0 ) )
        {
        return FALSE;
        }

    if(!(is_number(message[4])))
        {
        return FALSE;
        }

    i = 3;

    *hour = string_to_int(message,&i,&nbr_of_digits);

    if ( message[i] == ':' )
        {
        *min = string_to_int(message,&i,&nbr_of_digits);
        }

    if ( message[i] == ':' )
        {
        *sec = string_to_int(message,&i,&nbr_of_digits);
        }

    /* Check that time is valid */
    if ( (*hour > 23) || (*min > 59) || (*sec > 59) )
        {
        return FALSE;
        }

    return TRUE;
    }

uint8_t decode_day(char* message,uint8_t* day,uint8_t* month,uint16_t* year)
    {
    uint8_t i;
    uint8_t nbr_of_digits;

    *day = 0;
    *month = 0;
    *year = 0;

    if ( strncmp("DATE,",message,5) == 0 )
        {
        i = 4;
        }
    else if ( strncmp("PVM,",message,4) == 0 )
        {
        i = 3;
        }
    else
        {
        return FALSE;
        }

    if(!(is_number(message[i+1])))
        {
        return FALSE;
        }

    *day = string_to_int(message,&i,&nbr_of_digits);

    if ( message[i] == '.' )
        {
        *month = string_to_int(message,&i,&nbr_of_digits);
        }

    if ( message[i] == '.' )
        {
        *year = string_to_int(message,&i,&nbr_of_digits);
        }

    /* Check that date is valid */
    if ( (*day == 0) || (*month == 0) || (*year == 0) ||
         (*day > 31) || *month > 12)
        {
        return FALSE;
        }

    return TRUE;
    }

uint8_t decode_turn_intrv(char* message,uint8_t* turn_interval)
    {
    uint8_t i;
    uint8_t nbr_of_digits;

    *turn_interval = 0;

    if ( strncmp("INTERVAL,",message,9) == 0 )
        {
        i = 8;
        }
    else if ( strncmp("KAANTOVALI,",message,11) == 0 )
        {
        i = 10;
        }
    else
        {
        return FALSE;
        }

    if(!(is_number(message[i+1])))
        {
        return FALSE;
        }

    *turn_interval = string_to_int(message,&i,&nbr_of_digits);

    return TRUE;
    }

uint8_t decode_brightness(char* message,uint8_t* brightness)
    {
    uint8_t i;
    uint8_t nbr_of_digits;

    *brightness = 0;

    if ( strncmp("BRIGHTNESS,",message,11) == 0 )
        {
        i = 10;
        }
    else if ( strncmp("VALOISUUS,",message,10) == 0 )
        {
        i = 9;
        }
    else
        {
        return FALSE;
        }

    if(!(is_number(message[i+1])))
        {
        return FALSE;
        }

    *brightness = string_to_int(message,&i,&nbr_of_digits);

    if ( *brightness > 100 )
        {
        *brightness = 100;
        }

    return TRUE;
    }

uint8_t decode_worktime(char* message,uint8_t* first_working_month,uint8_t* last_working_month)
    {
    uint8_t i;
    uint8_t nbr_of_digits;

    *first_working_month = 0;
    *last_working_month = 0;

    if ( strncmp("WORKTIME,",message,9) == 0 )
        {
        i = 8;
        }
    else if ( strncmp("TOIMINTA,",message,9) == 0 )
        {
        i = 8;
        }
    else
        {
        return FALSE;
        }

    if(!(is_number(message[i+1])))
        {
        return FALSE;
        }

    *first_working_month = string_to_int(message,&i,&nbr_of_digits);

    if ( message[i] == '-' )
        {
        *last_working_month = string_to_int(message,&i,&nbr_of_digits);
        }

    /* Check that months are valid */
    if ( ( *first_working_month == 0 ) || ( *first_working_month > 12 )||
        ( *last_working_month == 0 ) || ( *last_working_month > 12) )
        {
        return FALSE;
        }

    return TRUE;
    }

uint8_t calculate_digits(char* message,uint8_t char_nbr)
    {
    uint8_t i = 0;

    /* Limit to 10 (uint32) */
    while ( (i < 11) && is_number(message[char_nbr]) )
        {
        i++;
        char_nbr++;
        }

    return i;
    }

uint32_t string_to_int(char* str,uint8_t* cursor,uint8_t* nbr_of_digits)
    {
    *nbr_of_digits = 0;
    char temp_digits_str[11];

    *cursor = *cursor + 1;
    *nbr_of_digits = calculate_digits(str,*cursor);
    clean_str(temp_digits_str,11);
    strncpy(temp_digits_str,str+*cursor,*nbr_of_digits);
    *cursor = *cursor+*nbr_of_digits;

    return atoi(temp_digits_str);
    }

void clean_str(char* str, uint8_t length)
    {
    uint8_t i;

    for ( i = 0; i < length; i++ )
        {
        str[i] = '\0';
        }
    }

uint8_t is_number(uint8_t character)
    {
    if ( ( character >= 48 ) && ( character <= 57 ) )
        {
        return TRUE;
        }
    else
        {
        return FALSE;
        }
    }