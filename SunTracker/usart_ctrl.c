#include <string.h>
#include "usart_ctrl.h"
#include "relay_ctrl.h"

ISR(USART_RXC_vect)
    {
    uint8_t character;

    character = UDR; /* Read data from register */

    if ( ( main_STM_ptr->stm_state == STM_STATE_INIT ) ||
        ( main_STM_ptr->stm_state == STM_STATE_CHECK_DATE ) ||
        ( main_STM_ptr->stm_state == STM_STATE_CHECK_BRIGHTNESS ) ||
        ( main_STM_ptr->stm_state == STM_STATE_UPDATE_TRACKER ) ||
        ( main_STM_ptr->stm_state == STM_STATE_MOVE_PANEL ) )
        {
        /* Input not allowed */
        return;
        }

    if (character == KEY_BS) /* Backspace */
        {
        USART_remove_from_buffer();
        }

    else if (character == KEY_CR) /* enter */
        {
        USART_Transmit(KEY_LF,NULL);   /* New line */
        USART_Transmit(KEY_CR,NULL);   /* CR, carridge return */
        USART_send_message();
        main_STM_ptr->new_message = TRUE;
        USART_buffer_clear();
        }
    else
        {
        main_STM_ptr->user_writing = TRUE;
        if (main_STM_ptr->stm_state != STM_STATE_SLEEP)
            {
            USART_add_to_buffer(character);
            }
        }
}

void USART_Init( STM_DATA_STR* STM_ptr )
    {

    main_STM_ptr = STM_ptr;

    /* Set baud rate */
    UBRRH = 0x00; /* write high register first */
    UBRRL = 2; /* 230.4k */

    UCSRB |= (1<<RXEN)|(1<<TXEN); // 0001 1000
    UCSRB |= (1<<RXCIE);
    /* Bit 4 � RXEN: Receiver Enable
     * Bit 3 � TXEN: Transmitter Enable
     * UCSRB |= 1<<RXCIE; // rx interrupt enable, 1xx1 1xxx
     * UCSRC = 0x86; // 8 Data, 1 Stop, No Parity 1xxx x11x
     */
    UCSRC |= (1<<URSEL)|(1<<UCSZ1)|(1<<UCSZ0);
    fdevopen (USART_Transmit, NULL);

    USART_buffer_clear();
    }

int USART_Transmit(char data, FILE *unused)
    {
    /* Wait for empty transmit buffer */
    while (!( UCSRA & (1<<UDRE)));
    /* UDRE is 1, when buffer is empty eg. message is sent */
    /* Put data into buffer, sends the data */
    UDR = data;

    return 0;
    }

void USART_buffer_clear(void)
    {
    uint8_t i;
    USART_buffer_index = 0;

    for (i=0; i<USART_BUFFER_SIZE; i++)
        {
        USART_buffer[i] = '\0';
        }
    main_STM_ptr->user_writing = FALSE;
    }

void USART_add_to_buffer(uint8_t character)
    {
    USART_buffer[USART_buffer_index] = character;

    if ( ++USART_buffer_index > 79 )
        {
        USART_buffer_index = 79;
        }
    else
        {
        USART_Transmit(character,NULL);
        }
    }

void USART_remove_from_buffer(void)
    {
    USART_buffer[USART_buffer_index] = '\0';

    USART_Transmit(KEY_BS,NULL);
    USART_Transmit(' ',NULL);
    USART_Transmit(KEY_BS,NULL);

    if ( USART_buffer_index != 0 )
        {
        USART_buffer_index--;
        }
    }

void USART_send_message(void)
    {
    uint8_t i = 0;

    while ( ( i < USART_BUFFER_SIZE ) && (USART_buffer[i] != '\0' ) )
        {
        main_STM_ptr->message[i] = USART_buffer[i];
        i++;
        }
    }