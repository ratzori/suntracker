#ifndef RAZ_DELAY_H_
#define RAZ_DELAY_H_

#include <avr/io.h>

void ms_delay( uint32_t );

#endif /* #ifndef RAZ_DELAY_H_ */