#include <stdio.h>
#include "timer_ctrl.h"
#include "common_data.h"

ISR(TIMER1_OVF_vect)
    {
    D( printf("Timer ISR\r\n") );
    timer_timeout = 1;
    TCCR1B = 0;
    }

void timer_init()
    {
    // TOIE1, timer1 overflow interrupt enable
    TIMSK |= 1 << TOIE1;
    TCCR1B = 0;
    sei();
    timer_timeout = 0;
    }

void timer_timeout_req(uint16_t timeout_sec)
    {
    uint16_t timer_init_time;

    TCCR1B = 0; /* stop timer */

    if (timeout_sec == 0)
        {
        /* Minimum time 1 sec */
        timeout_sec = 1;
        }

    timer_init_time = 0x0000;

    timer_init_time -= timeout_sec;

    TCNT1H = (uint8_t) (timer_init_time >> 8);
    TCNT1L = (uint8_t) (timer_init_time & 0xff);

    /* External clock source on T1 pin - falling edge */
    TCCR1B |= (1<<CS12) | (1<<CS11);
       timer_timeout = 0;
    }