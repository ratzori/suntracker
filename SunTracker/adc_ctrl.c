#include "adc_ctrl.h"

/*#define SIMULATOR*/

#ifdef SIMULATOR
uint8_t i=0;
#endif /* #ifdef SIMULATOR */

void adc_ctrl_init( void )
    {
    DDRA &= ~(1<<PA0); // j�nnitteen lukupinni "sis��n"
    DDRA &= ~(1<<PA1); // j�nnitteen lukupinni "sis��n"
    ADCSRA = (1<< ADEN) ; // ADC k�ytt��n
    ADMUX = 0; // ADC0
    ADMUX |= (1 << REFS0); // Uref = AVcc
    ADCSRA |= (1<<ADPS2) | (1<<ADPS0); // clk/32,  125 kHz
    ADCSRA |= (1<<ADSC); // aloita muunnos
    }

uint16_t adc_ctrl_get_sample( uint8_t port )
    {
    uint16_t sample;
#ifdef SIMULATOR
    const uint16_t sample_array[] = {480,490,493,499,497,500,492,478};
    if (++i > 7) i = 0;
    sample = sample_array[i];
#else /* #ifdef SIMULATOR */
    if (port > 7)
        {
        port = 7;
        }

    ADMUX = ( ADMUX & 0xF0 ) | port;

    loop_until_bit_is_set(ADCSRA, ADIF);
    ADCSRA |= (1 << ADIF) ; // nollaa lippu
    ADCSRA |= (1 << ADSC); // aloita uusi muunnos, eka hyl�t��n
    loop_until_bit_is_set(ADCSRA, ADIF);

    sample = ADCW;
#endif /* #ifdef SIMULATOR */
    return sample;
    }

uint16_t adc_ctrl_get_sample_rounded( uint8_t round_factor, uint8_t port )
    {
    uint8_t i;
    uint16_t sample = 0;

    for ( i=0; i < round_factor; i++ )
        {
        sample += adc_ctrl_get_sample(port);
        }
    sample /= round_factor;

    return sample;
    }

uint16_t adc_ctrl_scale_to( uint16_t adc_sample, uint16_t scalefactor )
    {
    uint16_t scaled_sample;

    scaled_sample = (uint16_t) ( (uint32_t) adc_sample * scalefactor / 1023 );

    return scaled_sample;
    }

uint16_t adc_ctrl_inverse(uint16_t adc_sample)
    {
    return (1023 - adc_sample);
    }