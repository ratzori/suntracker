#ifndef MOTOR_CTRL_H_
#define MOTOR_CTRL_H_

#include <avr/io.h>

#define MOTOR_SET_TIMEOUT 0
#define MOTOR_SET_OK 1

volatile uint16_t curr_pos;
volatile int16_t  pos_diff;

volatile uint8_t hysteresis;
volatile uint8_t pot_safe_margin;

uint16_t convert_to_motor_angle(uint16_t target_azimuth);
uint16_t convert_to_motor_angle_adc(uint16_t motor_angle);
void motor_set_safe_margin (uint8_t new_safe_margin);
void motor_set_hysteresis (uint8_t new_hysteresis);
uint8_t motor_set_position(uint16_t new_target_pos);

#endif /* #ifndef MOTOR_CTRL_H_ */