#ifndef USART_CTRL_H_
#define USART_CTRL_H_
#include "common_data.h"

#define XTAL = 8000000
#define BAUD = 9600

#define USART_BUFFER_SIZE (80 + 1) /* + NULL */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>

#define KEY_BS 0x08
#define KEY_LF 0x0A
#define KEY_CR 0x0D

STM_DATA_STR* main_STM_ptr;
uint8_t USART_buffer[USART_BUFFER_SIZE];
uint8_t USART_buffer_index;

void USART_Init( STM_DATA_STR* STM_ptr );
int USART_Transmit(char data, FILE *unused);
void USART_buffer_clear(void);
void USART_add_to_buffer(uint8_t character);
void USART_remove_from_buffer(void);
void USART_send_message(void);

#endif /* #ifndef USART_CTRL_H_ */