#ifndef TIMER_CTRL_H_
#define TIMER_CTRL_H_

#include <avr/io.h>
#include <avr/interrupt.h>

void timer_init(void);
void timer_timeout_req(uint16_t timeout_sec);

volatile uint8_t timer_timeout;

#endif /* #ifndef TIMER_CTRL_H_ */